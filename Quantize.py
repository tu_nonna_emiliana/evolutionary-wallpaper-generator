from sklearn.cluster import MiniBatchKMeans
import numpy as np
import cv2
from PIL import Image
from matplotlib import cm

def quantize(image, clusters) :
    image = np.array(image) # convert to numpy array, duh
    (h, w) = image.shape[:2] # get shape
    image = cv2.cvtColor(image, cv2.COLOR_BGR2LAB) #convert to LAB
    image = image.reshape((image.shape[0] * image.shape[1], 3)) # reshape into a feature vector
    # kmeans magic
    clt = MiniBatchKMeans(n_clusters = clusters)
    labels = clt.fit_predict(image)
    quant = clt.cluster_centers_.astype("uint8")[labels]
    # stop the magic
    quant = quant.reshape((h, w, 3)) # reshape image
    quant = cv2.cvtColor(quant, cv2.COLOR_LAB2BGR) # convert to RGB
    print(type(quant))
    result = Image.fromarray(quant) # convert from numpy array to image
    return result
