from random import randint as rint
from PIL import Image, ImageDraw

## my modules
from Mutations import *
from ImageAnalysis import *

def init(maxPopulation, width, height) :
    population = [ Image.new('RGB',(width,height)) for i in range(maxPopulation) ]
    population = [ addRectangle(i,rint(1,8)) for i in population ]
    return population

def fitness() :
    pass
    score = 0
    return score

def evaluate() :
    pass

def select() :

def alter() :
    pass

