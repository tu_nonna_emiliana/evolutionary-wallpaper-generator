import numpy as np
from PIL import Image, ImageDraw

### Mutations
## Unary mutations
def addRectangle(img, n) :
    draw = ImageDraw.Draw(img)
    for i in range(n) :
        xy = (rint(0,img.size[0]),rint(0,img.size[1]),\
                rint(0,img.size[0]),rint(0,img.size[1])) # (x0,y0, x1,y1)
        draw.rectangle(xy, fill = (rint(0,255),rint(0,255),rint(0,255)))
    return img
## Crossover Mutations
