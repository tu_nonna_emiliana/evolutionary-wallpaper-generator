import numpy as np
from PIL import Image
import argparse

## my modules
from GeneticAlgorithmsFlow import *
from ImageAnalysis import *
from Parse import *

## Main
# get arguments
args = parseArguments()
maxPopulation, generations, width, height = cleanInput(args)

# initialize population
population = init(maxPopulation, width, height)
#populationScore = eval(population)

# relax go nuts
for i in range(generations) :
    population = select(populationScore)
    population = alter(population)
    populationScore = evaluate()
