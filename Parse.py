import argparse

def parseArguments() :
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--population", required = False,type = int, help = "It's the number of images to be trained")
    parser.add_argument("-g", "--generations", required = False, type = int, help = "It's the number of generations the genetic algorithm must run for")
    parser.add_argument("-W", "--width", required = False, type = int, help = "It's the width of the generated images")
    parser.add_argument("-H", "--height", required = False, type = int, help = "It's the height of the generated images")
    args = parser.parse_args()
    return args

def cleanInput(args) :
    if not args.population :
        args.population = 256
    if not args.generations :
        args.generations = 256
    if not args.width :
        args.width = 640
    if not args.height :
        args.height = 480
    return args.population, args.generations, args.width, args.height
